<?php

class EconicAPI {
  function __construct($httpClass = 'cURL_API', $httpClassMethod = 'call') {
    $options = get_option('econic_api_settings');

    if (is_string($httpClass)) { $httpInstance = new $httpClass(); }
    if (is_object($httpClass)) { $httpInstance = $httpClass; }

    $this->http = [$httpInstance, $httpClassMethod];

    if (!$options || (is_array($options) && !array_key_exists('env', $options))) {
      throw new InvalidArgumentException('No environment specified with woo-econic-sandbox.');
    }

    if ($options['env'] === 'DEV') {
      $this->baseURL = 'https://dev.api.econic.ca';
    }

    if ($options['env'] === 'PROD') {
      $this->baseURL = 'https://api.econic.ca';
    }
  }

  function createOrder($data) {
    return call_user_func_array(
      $this->http,
      [$this->baseURL . '/orders', 'POST', $data]
    );
  }

  function updateOrder($data) {
    $id = $data['id'];

    return call_user_func_array(
      $this->http,
      [$this->baseURL . "/orders/$id", 'PUT', $data]
    );
  }

  function deleteOrder($data) {
    $id = $data['id'];

    return call_user_func_array(
      $this->http,
      [$this->baseURL . "/orders/$id", 'DELETE']
    );
  }
}
