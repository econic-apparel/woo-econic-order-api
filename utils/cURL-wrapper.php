<?php

class cURL_API {
  function call($url, $verb, $body = null) {
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, [
      'Accept: application/json',
      'Content-Type: application/json',
    ]);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $verb);

    if ($body !== null) {
      curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($body));
    }

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 30);

    $response = curl_exec($ch);
    $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    curl_close($ch);

    try {
      $json = json_decode($response, true);
      if ($json['message']) {
        $response = $json['message'];
      } else {
        $response = "Server response: " . $response;
      }
    } catch (Exception $e) {
        $response = "Server response: " . $response;
    }

    if ($code >= 400) {
      throw new Exception(
        "API returned $code response\n$response"
      );
    }

    return $response;
  }
}
