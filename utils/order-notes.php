<?php

class Econic_Order_Notes {
  public function __construct(\EconicAPI $api = null) {
    $this->api = $api;
  }

  public function getApi() {
    if ($this->api !== null) {
      return $this->api;
    } else {
      return new EconicAPI();
    }
  }

  private function doVerb($order, $apiMethod, $exceptionBase = 'Could not create this order on Econic API.\nReason:') {
    $note = '';

    try {
      $note = $this->getApi()->$apiMethod(transformOrder($order));
    } catch (Exception $e) {
      $message = $e->getMessage();
      $note = $exceptionBase . " " . $message;
    }

    $order->add_order_note($note);
  }

  public function onCreate($orderId, WC_Order $order) {
    $this->doVerb(
      $order,
      'createOrder',
      "Could not create this order on Econic API.\nReason:"
    );
  }

  public function onUpdate($orderId, WC_Order $order) {
    $this->doVerb(
      $order,
      'updateOrder',
      "Could not update this order on Econic API.\nReason:"
    );
  }

  public function onDelete($orderId, WC_Order $order) {
    $this->doVerb(
      $order,
      'deleteOrder',
      "Could not delete this order the on Econic API.\nReason:"
    );
  }
}
