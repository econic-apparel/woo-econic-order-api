<?php

function transformOrder(\WC_Abstract_Order $order) {
  $url = admin_url('/post.php?post=' . $order->get_id() . '&action=edit');

  return [
    'number' => $order->get_order_number(),
    'id' => substr(hash('sha256', $url), 0, 8),
    'url' => $url,
    'shipping_address' => [
      'company' => $order->get_shipping_company(),
      'first_name' => $order->get_shipping_first_name(),
      'last_name' => $order->get_shipping_last_name(),
      'address1' => $order->get_shipping_address_1(),
      'address2' => $order->get_shipping_address_2(),
      'city' => $order->get_shipping_city(),
      'province' => $order->get_shipping_state(),
      'zip' => $order->get_shipping_postcode(),
      'phone' => $order->get_shipping_phone(),
      'country' => $order->get_shipping_country(),
    ],
    'line_items' => array_values(array_map(
      function (\WC_Order_Item_Product $li) {
        $variant = wc_get_product($li->get_variation_id());

        return [
          'sku' => $variant->get_sku(),
          'quantity' => $li->get_quantity(),
          'subtotal' => $li->get_subtotal(),
          'total' => $li->get_total(),
          'taxes' => $li->get_taxes(),
        ];
      },
      $order->get_items(),
    )),
    'total' => $order->get_total(),
    'total_discount' => $order->get_total_discount(),
    'tax' => $order->get_total_tax(),
  ];
}
