<?php
/**
 * Plugin Name:     Woo Econic Order Api
 * Plugin URI:      https://gitlab.com/econic-apparel/woo-econic-order-api
 * Description:     Sets up a variety of hooks for a WordPress / WooCommerce site to interact with the Econic Orders API.
 * Author:          Cameron Hurd
 * Author URI:      https://cameronhurd.com
 * Text Domain:     woo-econic-order-api
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Woo_Econic_Order_Api
 */

require_once( plugin_dir_path(__FILE__) . '/utils/cURL-wrapper.php' );
require_once( plugin_dir_path(__FILE__) . '/utils/order-transform.php' );
require_once( plugin_dir_path(__FILE__) . '/utils/order-notes.php' );

require_once( plugin_dir_path(__FILE__) . '/options.php' );
require_once( plugin_dir_path(__FILE__) . '/banners.php' );
require_once( plugin_dir_path(__FILE__) . '/api.php' );

function ec_api_create_order($id, $order) {
  $notes = new Econic_Order_Notes();
  $notes->onCreate($id, $order);
}

function ec_api_update_order($id, $order) {
  $notes = new Econic_Order_Notes();

  if ('pending' !== $order->get_status()) {
    $notes->onUpdate($id, $order);
  }
}

function ec_api_delete_order($id, $order) {
  $notes = new Econic_Order_Notes();
  $notes->onDelete($id, $order);
}

add_action('woocommerce_order_status_pending_to_processing', 'ec_api_create_order', 10, 2);

/* add_action('woocommerce_update_order', 'ec_api_update_order', 10, 2); */
/* add_action('woocommerce_delete_order', 'ec_api_delete_order', 10, 2); */
