<?php

require_once('helpers/generate-dummy-order.php');

class WC_Econic_Test_Hooks extends WC_Unit_Test_Case {
  public function testWCNewOrderHookWhenMisconfigured() {
    $order = generateDummyOrder();
    $order->payment_complete();

    $notes = wc_get_order_notes(['order_id' => $order->get_id()]);

    $this->assertEquals(
      "Could not create this order on Econic API.\nReason: No environment specified with woo-econic-sandbox.",
      $notes[0]->content
    );
  }

  public function testWCNewOrderHookWhenWorking() {
    update_option('econic_api_settings', ['env' => 'DEV']);

    // Unhook the main Econic_Order_Notes::onCreate call
    // and splice in a dummy cURL response
    remove_action('woocommerce_order_status_pending_to_processing', 'ec_api_create_order');
    $mockCURL = $this->createMock(cURL_API::class);
    $mockCURL->method('call')->will(
      $this->returnValue('Test order recieved.')
    );

    $api = new EconicAPI($mockCURL);
    $notes = new Econic_Order_Notes($api);
    add_action('woocommerce_order_status_pending_to_processing', [$notes, 'onCreate'], 10, 2);

    $order = generateDummyOrder();
    $order->payment_complete();
    $notes = wc_get_order_notes(['order_id' => $order->get_id()]);

    $this->assertCount(2, $notes);
    $this->assertEquals('Test order recieved.', $notes[0]->content);
  }

  public function testWCNewOrderHookWhenFailing() {
    update_option('econic_api_settings', ['env' => 'DEV']);

    remove_action('woocommerce_order_status_pending_to_processing', 'ec_api_create_order');
    $mockCURL = $this->createMock(cURL_API::class);
    $mockCURL->method('call')->will(
      $this->throwException(new Exception('API returned 400 response'))
    );

    $api = new EconicAPI($mockCURL);
    $notes = new Econic_Order_Notes($api);
    add_action('woocommerce_order_status_pending_to_processing', [$notes, 'onCreate'], 10, 2);

    $order = generateDummyOrder();
    $order->payment_complete();
    $notes = wc_get_order_notes(['order_id' => $order->get_id()]);

    $this->assertCount(2, $notes);
    $this->assertEquals("Could not create this order on Econic API.\nReason: API returned 400 response", $notes[0]->content);
  }
}
