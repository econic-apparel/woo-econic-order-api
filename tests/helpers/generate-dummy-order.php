<?php

function generateDummyOrder() {
  $product = new WC_Product_Variable();

  $product->set_props([
    'name' => 'Men\'s Organic Cotton Short Sleeve - ',
    'sku' => 'EC1010',
  ]);

  $product->set_attributes([
    \WC_Helper_Product::create_product_attribute_object('size', ['XS', 'S', 'M', 'L', 'XL', '2XL']),
    \WC_Helper_Product::create_product_attribute_object('colour', ['Black', 'Midnight Blue']),
  ]);

  $product = wc_get_product($product->save());

  $variations = [
    \WC_Helper_Product::create_product_variation_object(
      $product->get_id(),
      'EC1010-XS-BK',
      12,
      [
        'pa_size'   => 'XS',
        'pa_colour' => 'Black',
      ]
    )
  ];

  $variation_ids = array_map(
    function( $variation ) {
      return $variation->get_id();
    },
    $variations
  );

  $product->set_children($variation_ids);
  $product->save();

  $order = wc_create_order([
    'status'        => 'pending',
    'customer_id'   => 1,
    'customer_note' => '',
    'total'         => '',
  ]);

  $orderItem = new \WC_Order_Item_Product();
  $orderItem->set_props([
    'product'  => $variations[0],
    'quantity' => 4,
    'subtotal' => wc_get_price_excluding_tax( $variations[0], array( 'qty' => 4 ) ),
    'total'    => wc_get_price_excluding_tax( $variations[0], array( 'qty' => 4 ) ),
  ]);

  $orderItem->save();
  $order->add_item($orderItem);

  // Set billing address.
  $order->set_billing_first_name( 'Jeroen' );
  $order->set_billing_last_name( 'Sormani' );
  $order->set_billing_company( 'WooCompany' );
  $order->set_billing_address_1( 'WooAddress' );
  $order->set_billing_address_2( '' );
  $order->set_billing_city( 'WooCity' );
  $order->set_billing_state( 'NY' );
  $order->set_billing_postcode( '12345' );
  $order->set_billing_country( 'US' );
  $order->set_billing_email( 'admin@example.org' );
  $order->set_billing_phone( '555-32123' );

  // Set shipping address.
  $order->set_shipping_first_name( 'Jeroen' );
  $order->set_shipping_last_name( 'Sormani' );
  $order->set_shipping_company( 'WooCompany' );
  $order->set_shipping_address_1( 'WooAddress' );
  $order->set_shipping_address_2( '' );
  $order->set_shipping_city( 'WooCity' );
  $order->set_shipping_state( 'NY' );
  $order->set_shipping_postcode( '12345' );
  $order->set_shipping_country( 'US' );
  $order->set_shipping_phone( '555-32123' );

  $order->save();

  return $order;
}
