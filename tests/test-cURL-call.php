<?php

class WC_Econic_Test_API extends WC_Unit_Test_Case {
  private function getMockCURL() {
    return $this->getMockBuilder(cURL_API::class)
                ->setMethods(['call'])
                ->getMock();
  }

  /**
   * No 'econic_api_settings' option stored in DB.
   *
   * @expectedException InvalidArgumentException
   */
  public function testMisconfigured() {
    $api = new EconicAPI();
  }

  public function testApiConfig() {
    add_option('econic_api_settings', ['env' => 'PROD']);
    $api = new EconicAPI();
    $this->assertEquals($api->baseURL, 'https://api.econic.ca');

    update_option('econic_api_settings', ['env' => 'DEV']);
    $devApi = new EconicAPI();
    $this->assertEquals($devApi->baseURL, 'https://dev.api.econic.ca');
  }

  public function testCreate() {
    add_option('econic_api_settings', ['env' => 'DEV']);

    $mockCURL = $this->getMockCURL();
    $mockCURL->expects($this->once())
             ->method('call')
             ->with('https://dev.api.econic.ca/orders', 'POST', ['foo' => 'bar']);

    $api = new EconicAPI($mockCURL);

    $api->createOrder(['foo' => 'bar']);
  }

  public function testUpdate() {
    add_option('econic_api_settings', ['env' => 'DEV']);

    $mockCURL = $this->getMockCURL();
    $mockCURL->expects($this->once())
             ->method('call')
             ->with('https://dev.api.econic.ca/orders/100', 'PUT', ['foo' => 'bar', 'id' => 100]);

    $api = new EconicAPI($mockCURL);

    $api->updateOrder(['foo' => 'bar', 'id' => 100]);
  }

  public function testDelete() {
    add_option('econic_api_settings', ['env' => 'DEV']);

    $mockCURL = $this->getMockCURL();
    $mockCURL->expects($this->once())
             ->method('call')
             ->with('https://dev.api.econic.ca/orders/100', 'DELETE');

    $api = new EconicAPI($mockCURL);

    $api->deleteOrder(['id' => 100]);
  }
}
