<?php

require_once('helpers/generate-dummy-order.php');

class WC_Econic_TransformOrder extends WC_Unit_Test_Case {
  public function testTransform() {
    $order = generateDummyOrder();

    $transformedOrder = \transformOrder($order);

    // Should all be 8 char hashes
    $this->assertEquals(
      strlen($transformedOrder['id']),
      8
    );

    $this->assertArrayHasKey(
      'number',
      $transformedOrder
    );

    foreach (['total', 'total_discount', 'tax'] as $number) {
      $this->assertFalse(
        'string' === gettype($transformedOrder[$number])
      );
    }

    $this->assertEquals(
      [
        [
          'sku' => 'EC1010-XS-BK',
          'quantity' => 4,
          'total' => '48',
          'subtotal' => '48',
          'taxes' => [
            'subtotal' => [],
            'total' => [],
          ],
        ],
      ],
      $transformedOrder['line_items'],
    );

    $this->assertEquals(
      [
        'company' => 'WooCompany',
        'first_name' => 'Jeroen',
        'last_name' => 'Sormani',
        'address1' => 'WooAddress',
        'address2' => '',
        'city' => 'WooCity',
        'province' => 'NY',
        'country' => 'US',
        'phone' => '555-32123',
        'zip' => '12345'
      ],
      $transformedOrder['shipping_address'],
    );
  }
}
