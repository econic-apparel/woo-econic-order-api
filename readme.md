# Woo Econic Order Api

[![Pipeline](https://gitlab.com/econic-apparel/woo-econic-order-api/badges/master/pipeline.svg)](https://gitlab.com/econic-apparel/woo-econic-order-api/-/commits/master)
[![Coverage](https://gitlab.com/econic-apparel/woo-econic-order-api/badges/master/coverage.svg)](https://gitlab.com/econic-apparel/woo-econic-order-api/-/commits/master)

- Tags: econic-order-api
- Requires at least: 4.5
- Tested up to: 5.8.1
- Requires PHP: 5.6+
- Stable tag: 0.1.0
- License: GPLv2
- License URI: https://www.gnu.org/licenses/gpl-2.0.html

Sets up a variety of hooks for a WordPress / WooCommerce site to interact with the Econic Orders API.

## Description

We'll support crud operations, located at the following hooks:

- `woocommerce_order_status_pending_to_processing`
- `woocommerce_update_order` (conditionally)
- `woocommerce_delete_order` / `woocommerce_trash_order`

## Installation

- Upload a zip from the Releases page, or manually upload `woo-econic-order-api.php` to the `/wp-content/plugins/` directory
- Activate the plugin through the 'Plugins' menu in WordPress
- Set your API key in the 'settings' tab.

## Changelog

### 0.3
Actual POST requests to the API on

### 0.2
Tests to generate a sane output to later POST at hooked locations

### 0.1
Initial scaffolding & proof of concept

## TODO

- [ ] Qualify those requests w/ saved API Key(s)
- [ ] Save timestamp `onCreate` for comparison with `onUpdate` op.
- [ ] Hook up `onUpdate` && `onDelete`
