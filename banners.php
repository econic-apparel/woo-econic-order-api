<?php

function econic_api_notices() {
  $options = get_option('econic_api_settings');

  $econic_settings = wp_parse_args(
    $options,
    [
      'env' => 'DEV',
      'api_key' => [
        'prod' => '',
        'dev' => '',
      ]
    ]
  );

  $env = $econic_settings['env'];
  $prodKey = $econic_settings['api_key']['prod'];
  $devKey = $econic_settings['api_key']['dev'];

  $settingsUrl = get_admin_url(null, '/options-general.php?page=econic-api-settings');

  if ($env === 'PROD' && $prodKey === '') {
    add_action('admin_notices', function() {

      echo '<div class="notice notice-error">
        <p>The Econic Order API plugin is enabled in production mode, but there is no production API key set.</p>
        <p>Please visit <a href="' . $settingsUrl . '">the settings page</a> to set the correct values.</p>
        <p>Without an API key, orders will not get sent to the API properly.</p>
      </div>';
    });
  }

  if ($env === 'DEV' && $devKey === '') {
    add_action('admin_notices', function() {
      echo '<div class="notice notice-warning">
        <p>The Econic Order API plugin is enabled in development mode, but there is no development API key set.</p>
        <p>Please visit <a href="' . $settingsUrl . '">the settings page</a> to set the correct values.</p>
        <p>Without an API key, orders will not get sent to the API properly.</p>
      </div>';
    });
  }
}

add_action('admin_init', 'econic_api_notices');
