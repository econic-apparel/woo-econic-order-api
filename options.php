<?php

function econic_api_add_settings_page() {
  add_options_page(
    'Econic API Settings',
    'Econic API',
    'manage_options',
    'econic-api-settings',
    'econic_api_render_settings_page'
  );
}

function econic_api_render_settings_page() {
?>
  <h2>Econic API Plugin Settings</h2>
  <form action="options.php" method="post">
    <?php
    settings_fields('econic_api_credentials');
    do_settings_sections('econic-api-settings');
    ?>
    <input
      type="submit"
      name="submit"
      class="button button-primary"
      value="<?php esc_attr_e( 'Save' ); ?>"
    />
  </form>
<?php
}

function econic_api_register_settings() {
  register_setting(
    'econic_api_credentials',
    'econic_api_settings',
  );

  add_settings_section(
    'credentials',
    'Credentials',
    'econic_api_render_credentials_settings',
    'econic-api-settings'
  );
}

function econic_api_render_credentials_settings() {
  $options = get_option('econic_api_settings');
?>
  <label>Environment</label>
  <ul style="padding-left: 10px">
    <li>
      <label for="prod">
          <input id="prod" type="radio"
            name="<?php echo esc_attr('econic_api_settings[env]') ?>"
            value="PROD" <?php checked($options['env'], 'PROD') ?>
          />
          Production
      </label>
      <br />
      <label for="dev">
          <input id="dev" type="radio"
            name="<?php echo esc_attr('econic_api_settings[env]') ?>"
            value="DEV" <?php checked($options['env'], 'DEV') ?>
          />
          Development
      </label>
    </li>
  </ul>
  <p>
    <label for="prod_key">Production API Key:</label><br />
    <input id="prod_key" type="text" name="<?php echo esc_attr('econic_api_settings[api_key][prod]') ?>" value="<?php echo $options['api_key']['prod']; ?>" />
  </p>
  <p>
    <label for="dev_key">Development API Key:</label><br />
    <input id="dev_key" type="text" name="<?php echo esc_attr('econic_api_settings[api_key][dev]') ?>" value="<?php echo $options['api_key']['dev']; ?>" />
  </p>
  <p>
    <small>
      Please visit <a href="https://econic-api-console.vercel.app/" target="_BLANK">econic-api-console.vercel.app</a> if you do not have this info.
    </small>
  </p>
<?php
}

add_action('admin_menu', 'econic_api_add_settings_page');
add_action('admin_init', 'econic_api_register_settings');

